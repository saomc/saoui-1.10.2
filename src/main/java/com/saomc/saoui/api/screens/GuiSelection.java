package com.saomc.saoui.api.screens;

/**
 * This is the choice of GUI's available for elements to display on
 * <p>
 * Created by Tencao on 31/07/2016.
 */
public enum GuiSelection {
    IngameMenuGUI,
    DeathScreen,
    MainMenuGUI
}
