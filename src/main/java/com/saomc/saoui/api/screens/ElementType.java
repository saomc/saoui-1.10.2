package com.saomc.saoui.api.screens;

/**
 * This helps quickly identify the type of element rather than doing several checks
 */
public enum ElementType {
    MENU,
    SLOT,
    INVENTORY,
    ITEM,
    PLAYER,
    OPTION
}
