package com.saomc.saoui.colorstates;

public enum CursorStatus {

    SHOW,
    DEFAULT,
    HIDDEN

}
