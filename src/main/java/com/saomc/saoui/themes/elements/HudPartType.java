package com.saomc.saoui.themes.elements;

/**
 * Part of saoui by Bluexin.
 *
 * @author Bluexin
 */
public enum HudPartType {

    /**
     * Key for the HudPart rendering the HP box on-screen.
     */
    HEALTH_BOX,

    /**
     * Key for the HudPart rendering the hotbar parts
     */
    HOTBAR
}
