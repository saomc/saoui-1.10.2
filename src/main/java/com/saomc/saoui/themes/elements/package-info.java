/**
 * Part of saoui by Bluexin.
 *
 * @author Bluexin
 */
@XmlAccessorType(value = XmlAccessType.FIELD)
package com.saomc.saoui.themes.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
